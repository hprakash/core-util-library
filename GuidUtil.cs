﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HPrakash.Library
{
    public class GuidUtil
    {
        public static Guid GetNewGuid()
        {
            return Guid.NewGuid();
        }
    }
}
